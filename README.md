# Dotfiles

![BSPWM Screenshot with ranger file manager, fzf fuzzy finder, and neovim text editor](./.pictures/BSPWM.png)

My dotfiles for Archlinux, on BSPWM windows manager.

* Text Editor:       **neovim**
* Shell:             **fish** run inside **xst** terminal emulator
* Fuzzy finder:      with **fzf** and the_silver_searcher integration, that's pretty cool
* File manager:      **ranger**, images are displayed
* Music:             **mpd** with *ncccccmmmpppcccppppppp*

* Window Manager:    **BSPWM**
* Window Compositor: **compton** for blur, transparency and shadows

# Installation

Install the list of packages from ./package_list.txt
Those are the name from Archlinux, feel free to adapt for your distribution.

Copy the configuration files as desired

## Fish

Install and run fish. I set Zsh as my main shell and run fish inside. fish is not POSIX, it is better to do it this way.
You can find the corresponding line in `.zshrc`

Install [oh-my-fish](https://github.com/oh-my-fish/oh-my-fish) to change theme of fish

```
curl -L https://get.oh-my.fish | fish
```

## Neovim

Neovim is a text editor, a powerful one. vi<vim<nvim proves that neovim is better.
Please use Neovim in place of Vim as it improves it with functionnality that are actually in *Emacs* (the evil lisp operating system).
Configuration file, using vim plug package manager

Install vim-plug
```bash
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

Copy and paste configuration, then open with Neovim (nvim executable)
`.config/nvim/init.vim`

In vim `:` is in fact the Escape key

:source %
:PlugInstall

Neovim is now configured
