# vim: set ft=fish :
## Hello
set fish_greeting                 #  Shell is impolite, do not greet user
set -x EDITOR nvim                #  Nvim master race
set -U GOPATH "$HOME/go"          #  CIA agent programming language of the botnet

# Bug fix
# bspc config border_radius 10

export TERM=xterm-256color
export PATH="$PATH:$HOME/go/bin"
export PATH="$PATH:$HOME/bin"
export PATH="$PATH:$HOME/.local/bin"
export SCRCPY_SERVER_PATH=/usr/share/scrcpy/scrcpy-server-v1.9.jar

## Fuzzy finder
set -x FZF_LEGACY_KEYBINDINGS 0   #  Black magic for fuzzy finder in shell and vim
set -x FZF_DEFAULT_OPTS '--color fg:242,bg:236,hl:65,fg+:15,bg+:239,hl+:108,info:108,prompt:109,spinner:108,pointer:168,marker:168'
set -x FZF_DEFAULT_COMMAND 'rg --files --hidden --follow --glob "!.git/*"'
set -x FZF_FIND_FILE_COMMAND 'rg --files --hidden --follow --glob "!.git/*"'
# set -x ANSIBLE_VAULT_PASSWORD_FILE '~/.vault_pass.txt'

## Pacman
alias upd8='yay -Syu' # System updates
alias rekt='yay -Rcs' # delete package
alias o='yay'         # pacman wrapper
alias k='yay -S'      # install package
#alias docker='podman'

alias v='xbpsui'

## Games
# set -x STEAM_RUNTIME 0      # To play games
set -x SDL_AUDIODRIVER alsa # Alsa>pulseaudio

## Utilities
alias music='tmux new-session "tmux source-file ~/.ncmpcpp/tmux_session"' # Tmux session with ncmpcpp and cover art
alias manga='gallery-dl'                                                  # Weeb anime bullshit
alias yt='youtube-dl -f bestvideo+bestaudio'                              # Youtube videos
alias nico='youtube-dl'                              # Youtube videos
alias pacgraph='pacgraph -b "#111111" -t "#ffffff" -d "#1793d1" -p 9 55'
alias terebi="minidlnad -f $HOME/.config/minidlna/minidlna.conf -P $HOME/.config/minidlna/minidlna.pid"

## Navigation
alias ls='exa'   # ls with colors
alias lla='exa -la'

## Git
alias g:i='git init'
alias g:c='git commit -a'
alias g:a='git add -A'
alias g:s='git status'
alias g:l='git pull'
alias g:p='git push'
alias g:d='git diff'

## Docker compose
alias d:i='docker-compose inspect'
alias d:p='docker-compose ps'
alias d:u='docker-compose up'
alias d:d='docker-compose down'
alias d:l='docker-compose logs'
alias d:s='docker-compose stop'
alias d:r='docker-compose rm -vf'

alias k8s='sudo kubectl'

## Functions
function git-replicate --description 'Copy git repositorie to new origin'
     git push origin +refs/remotes/origin/\*:refs/heads/\*
     git push origin --tags
end

function yta --description 'Find and stream youtube audio'
	mpv --ytdl-format=bestaudio ytdl://ytsearch:"$argv"
end

function r
    if test -z $RANGER_LEVEL
        ranger $argv
    else
        exit
    end
end

cat ~/.cache/wal/sequences
